const BufferPlus = require('buffer-plus');

enum SCHEMA {
	ON_STAGE_GAME_START = "g6OnStageGameStart",
	ON_SOMEONE_DRAW = "g6OnSomeoneDraw",
}

BufferPlus.createSchema(SCHEMA.ON_STAGE_GAME_START, {
	description: "開局",
	type: "object",
	order: ["timestamp", "dice", "roomRule"],
	properties: {
		timestamp: {
			description: "狀態結束時間",
			type: "uint64le",
		},
		dice: {
			description: "",
			type: "array",
			items: {
				description: "",
				type: "uint64le",
			},
		},
		roomRule: {
			description: "房間規則",
			type: "object",
			order: ["themeType", "actionTime", "betBase"],
			properties: {
				themeType: {
					description: "廳別",
					type: "uint8",
				},
				actionTime: {
					description: "行為思考時間",
					type: "uint8",
				},
				betBase: {
					description: "番型底分",
					type: "uint64le",
				},
			},
		},
	},
})

BufferPlus.createSchema(SCHEMA.ON_SOMEONE_DRAW, {
	description: "摸牌",
	type: "object",
	order: ["drawTilePlayerSeatID", "tileData", "nowDeck"],
	properties: {
		drawTilePlayerSeatID: {
			description: "摸牌玩家座位ID",
			type: "uint8",
		},
		tileData: {
			description: "",
			type: "object",
			order: ["type", "digit"],
			properties: {
				type: {
					description: "種類",
					type: "uint8",
				},
				digit: {
					description: "點數",
					type: "uint8",
				},
			},
		},
		nowDeck: {
			description: "牌山剩餘量",
			type: "uint8",
		},
	},
})

