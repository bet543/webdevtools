FROM node:10.20.1-stretch-slim

COPY . .

RUN npm install
RUN npm run build

ENTRYPOINT ["node", "./server/index.js"]
