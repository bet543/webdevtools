import Vue from 'vue'
import Router from 'vue-router'
import SDKParse from "../components/SDKParse"
import Mahjong from "../components/Mahjong/Mahjong"

Vue.use(Router)

export default new Router({
	mode: 'history',
	base: "./",
	routes: [
		{
			path: "/SDKParse",
			component: SDKParse,
			content: "SDK Generator",
		},
		{
			path: "/Mahjong",
			component: Mahjong,
			content: "配牌器",
		}
	]
})
