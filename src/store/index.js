import Vue from 'vue'
import Vuex from 'vuex'
import schema from './modules/schema'
import mahjong from './modules/mahjong'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    schema,
    mahjong,
  },
  strict: debug,
})