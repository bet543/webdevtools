import { v4 as uuid } from "uuid";
import api from "../../api/api";
// initial state
const state = {
	schemaFields: [{
		id: uuid(),
		schema: { 
			name: "", 
			code: "",
			desc: "",
		},
		structure: {
			type: "object",
			content: [],
		}
	}],
}

const getAttributeByID = (index, data, attributesID) => {
	const id = attributesID[index];
	const attribute = data.find(item => item.id === id);

	if(index === attributesID.length - 1) {
		return attribute;
	}
	else {
		return getAttributeByID(index + 1, attribute.content, attributesID);
	}
}

// getters
const getters = {
	getSchema: (state) => {
		return state.schemaFields;
	},

	/**
	 * 取得content底下所有的attribute
	 */
	getAttributesBySchema:(state, getters, rootState) => (schemaID, attributesID) => {
		const field = state.schemaFields.find(item => item.id === schemaID);

		if(attributesID.length === 0) {
			return field.structure;
		}
		else {
			return getAttributeByID(0, field.structure.content, attributesID);
		}
	},
}

// actions
const actions = {
	addSchema: ({commit}) => {
		commit('addSchema');
	},
	deleteSchema: ({commit}, id) => {
		commit('deleteSchema', id);
	},
	setSchemaName: ({state, commit}, data) => {
		commit("setSchemaName", data)
	},
	setSchemaCode: ({state, commit}, data) => {
		commit("setSchemaCode", data)
	},
	setSchemaDesc: ({state, commit}, data) => {
		commit("setSchemaDesc", data)
	},
	addAttribute: ({state, commit}, data) => {
		commit("addAttribute", data)
	},
	deleteAttribute: ({state, commit}, data) => {
		commit("deleteAttribute", data)
	},
	setAttributeName: ({state, commit}, data) => {
		commit("setAttributeName", data)
	},
	setAttributeType: ({state, commit}, data) => {
		commit("setAttributeType", data)
	},
	setAttributeDesc: ({state, commit}, data) => {
		commit("setAttributeDesc", data)
	},
	generate: ({state, commit}) => {
		console.log(state.schemaFields);
		api.sdkParse(state.schemaFields);
	},
}

// mutations
const mutations = {
	addSchema: (state, products) => {
		state.schemaFields.push({
			id: uuid(),
			schema: { 
				name: "", 
				code: "",
				desc: "",
			},
			structure: {
				type: "object",
				content: [],
			}
		});

		console.log(state);
	},
	deleteSchema: (state, id) => {
		const index = state.schemaFields.findIndex(item => item.id === id);

		state.schemaFields.splice(index, 1);
	},
	setSchemaName: (state, data) => {
		const { schemaID, schemaName } = data;
		const field = state.schemaFields.find(item => item.id === schemaID);

		field.schema.name = schemaName;
	},
	setSchemaCode: (state, data) => {
		const { schemaID, schemaCode } = data;
		const field = state.schemaFields.find(item => item.id === schemaID);

		field.schema.code = schemaCode;
	},
	setSchemaDesc: (state, data) => {
		const { schemaID, schemaDesc } = data;
		const field = state.schemaFields.find(item => item.id === schemaID);

		field.schema.desc = schemaDesc;
	},
	addAttribute: (state, {schemaID, attributesID}) => {
		const field = state.schemaFields.find(item => item.id === schemaID);
		const id = uuid();

		if(attributesID.length === 0) {
			field.structure.content.push({
				attributesID: [id],
				id: id,
				name: "",
				type: 0,
				desc: "",
				content: [],
			})
		}
		else {
			let attribute = getAttributeByID(0, field.structure.content, attributesID);

			attribute.content.push({
				attributesID: [...attributesID, id],
				id: id,
				name: "",
				type: 0,
				desc: "",
				content: [],
			})
		}
	},
	deleteAttribute: (state, {schemaID, attributesID}) => {
		const field = state.schemaFields.find(item => item.id === schemaID);
		if(attributesID.length === 1) {
			const index = field.structure.content.findIndex(item => item.id === attributesID[0]);

			field.structure.content.splice(index, 1);
		}
		else {
			const attribute = getAttributeByID(0, field.structure.content, attributesID.slice(0, attributesID.length - 1));
			const index = attribute.content.findIndex(item => item.id === attributesID.slice(-1)[0]);
	
			attribute.content.splice(index, 1);
		}
	},
	setAttributeName: (state, { schemaID, attributeName, attributesID }) => {
		const field = state.schemaFields.find(item => item.id === schemaID);
		const attribute = getAttributeByID(0, field.structure.content, attributesID);

		attribute.name = attributeName;
	},
	setAttributeType: (state, { schemaID, attributeType, attributesID }) => {
		const field = state.schemaFields.find(item => item.id === schemaID);
		const attribute = getAttributeByID(0, field.structure.content, attributesID);

		attribute.type = attributeType;

		switch (attributeType) {
			case "array":
			case "object":
				const id = uuid();

				attribute.content.push({
					attributesID: [...attributesID, id],
					id: id,
					name: "",
					type: 0,
					desc: "",
					content: [],
				});
				break;
			default:
				attribute.content = [];
				break;
		}
	},
	setAttributeDesc: (state, { schemaID, attributeDesc, attributesID }) => {
		const field = state.schemaFields.find(item => item.id === schemaID);
		const attribute = getAttributeByID(0, field.structure.content, attributesID);

		attribute.desc = attributeDesc;
	},
}

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}
