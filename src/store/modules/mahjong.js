import api from "../../api/api";

const MAX_TILE = 4;
// initial state
const state = {
    documentList: [],
    document: {
        wall: [],
        handList: [
            [],
            [],
            [],
            [],
        ],
        diceList: [3, 3, 2],
    },
    leftTilesNum: {},
};

// getters
const getters = {
    getDocumentList: (state) => {
        return state.documentList;
    },
    getBanker: (state) => {
        const total = state.document.diceList.reduce((accumulator, currentValue) => accumulator + currentValue);

        return Math.round((total - 1) % 4);
    },
    getDice: (state) => {
        return state.document.diceList || [];
    },
    getWall: (state) => {
        return state.document.wall || [];
    },
    getHandTiles: (state) => (seatID) => {
        return state.document.handList[seatID] || [];
    },
    getLefTilesNum: (state) => {
        return state.leftTilesNum;
    },
    // 防呆檢查
    islegalTiles: (state) => (payload) => {
        // seatID  0: 本家  1: 下家  2: 對家  3: 上加  -1: 牌牆
        const { tileList, seatID } = payload;

        let tmpList = {};
        let legal = true;
        let reason = -1;

        const addTmp = item => {
            const key = "id_" + item;

            if (tmpList[key]) {
                tmpList[key]++;
            }
            else {
                tmpList[key] = 1;
            }
        }

        tileList.map(addTmp);

        if(seatID !== -1){
            state.document.wall.map(addTmp);
        }

        state.document.handList.map((handTiles, index) => {
            if(index === seatID){
                return;
            }

            handTiles.map(addTmp);
        })

        Object.keys(tmpList).map(key => {
            if(tmpList[key] > MAX_TILE) {
                legal = false;
                reason = key.replace("id_", "");
            }
        })

        return {
            legal,
            reason
        };
    }
};

// actions
const actions = {
    queryDocumentList: ({ state, commit }) => {
        return new Promise((resolve, reject) => {
            api.queryDocumentList().then((data) => {
                commit("setDocumentList", data);
                resolve(data);
            });
        })
    },
    createDocument: ({state, commit}, payload) => {
        return new Promise((resolve, reject) => {
            api.createDocument(payload).then((data) => {
                resolve(data);
            })
            .catch((err) => {
                reject(err);
            });
        })
    },
    deleteDocument: ({ state, commit }, payload) => {
        return new Promise((resolve, reject) => {
            api.deleteDocument(payload).then((data) => {
                resolve(data);
            })
            .catch((err) => {
                reject(err);
            });
        })
    },
    queryDocument: ({ state, commit }, payload) => {
        api.queryDocument(payload).then((data) => {
            commit("setDocumentData", data);
        });
    },
    updateTiles: ({ state, commit }, payload) => {
        api.updateTiles(payload).then(data => {
            commit("setDocumentData", data);
        })
    },
    updateDice: ({ state, commit }, payload) => {
        const { index, value, key } = payload;

        state.document.diceList[index] = value;
        api.updateDice({
            key,
            data: { diceList: state.document.diceList }
        }).then(data => {
            commit("setDocumentData", data);
        })
    },
};

// mutations
const mutations = {
    setDocumentList: (state, data) => {
        state.documentList = data;
    },
    setDocumentData: (state, data) => {
        state.document = data;

        for (let i = 1; i < 10; i++) {
			state.leftTilesNum["id_" + i] = MAX_TILE;
		}
		for (let i = 11; i < 20; i++) {
			state.leftTilesNum["id_" + i] = MAX_TILE;
		}
		for (let i = 21; i < 30; i++) {
			state.leftTilesNum["id_" + i] = MAX_TILE;
		}
		for (let i = 31; i < 38; i++) {
			state.leftTilesNum["id_" + i] = MAX_TILE;
		}

        state.document.wall.map(item => {
            state.leftTilesNum["id_" + item]--;
        })
        state.document.handList.map(tiles => {
            tiles.map((item) => {
                state.leftTilesNum["id_" + item]--;
            })
        });
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}