import axios from 'axios';

export default {
	sdkParse(data) {
		axios.post("/api/SDK/parse", {
			data
		}).then((response) => {
			console.log("response => ", response);
			const fileURL = window.URL.createObjectURL(new Blob([response.data]));
			const fileLink = document.createElement('a');

			fileLink.href = fileURL;
			fileLink.setAttribute('download', 'file.ts');
			document.body.appendChild(fileLink);

			fileLink.click();
		}).catch((error) => {
			console.log("error => ", error);
		})
	},
	createDocument(key) {
		return new Promise((resolve, reject) => {
			axios.post("/api/Mahjong/G6", { key }).then((response) => {
				console.log("response => ", response);
				const { result, msg } = response.data;
				
				if(result === 0) {
					resolve(response.data);
				}
				else {
					reject(msg);
				}
			}).catch((error) => {
				console.log("error => ", error);
				reject(error);
			})
		})
	},
	deleteDocument(key) {
		return new Promise((resolve, reject) => {
			axios.delete("/api/Mahjong/G6/" + key).then((response) => {
				console.log("response => ", response);
				const { result, msg } = response.data;
				
				if(result === 0) {
					resolve(response.data);
				}
				else {
					reject(msg);
				}
			}).catch((error) => {
				console.log("error => ", error);
				reject(error);
			})
		})
	},
	queryDocumentList() {
		return new Promise((resolve, reject) => {
			axios.get("/api/Mahjong/G6").then((response) => {
				console.log("response => ", response);
				resolve(response.data);
			}).catch((error) => {
				console.log("error => ", error);
				reject(error);
			})
		})
	},
	queryDocument(key) {
		return new Promise((resolve, reject) => {
			axios.get("/api/Mahjong/G6/" + key).then((response) => {
				console.log("response => ", response);
				resolve(response.data);
			}).catch((error) => {
				console.log("error => ", error);
				reject(error);
			})
		})
	},
	updateTiles(param) {
		return new Promise((resolve, reject) => {
			axios.put("/api/Mahjong/G6/", {
				param
			}).then((response) => {
				console.log("response => ", response);
				resolve(response.data);
			}).catch((error) => {
				console.log("error => ", error);
				reject(error);
			})
		})
	},
	updateDice(param) {
		return new Promise((resolve, reject) => {
			axios.put("/api/Mahjong/G6/", {
				param
			}).then((response) => {
				console.log("response => ", response);
				resolve(response.data);
			}).catch((error) => {
				console.log("error => ", error);
				reject(error);
			})
		})
	}
}
