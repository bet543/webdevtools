import axios from 'axios';

export const createInstance = (baseUrl, headers = {}) => {
	return axios.create({
		baseUrl,
		headers,
	})
}
