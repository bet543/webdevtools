const fs = require("fs");
const parse = require("./helper/parse");

fs.readFile("./schema.json", "utf8", (err, jsonString) => {
    if (err) {
        console.log(err);
        return;
    }

    const data = JSON.parse(jsonString);
    const schemaString = parse(data);

    fs.writeFile('./example.ts', schemaString, "utf8", err => {
        if (err) {
            console.log('Error writing file', err)
        } else {
            console.log('Successfully wrote file')
        }
    });
})