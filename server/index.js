const api = require('./api/api');
const fs = require('fs');
const path = require('path');
const bodyParser = require('body-parser')
const express = require('express');
const db = require("./db");
const app = express();
const { PORT } = require("./settings");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(api);
app.use(express.static(path.resolve(__dirname, '../dist')))
app.get('*', function (req, res) {
	const html = fs.readFileSync(path.resolve(__dirname, '../dist/index.html'), 'utf-8')
	res.send(html)
})

app.listen(PORT);
console.log('success listen…………');