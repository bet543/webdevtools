const fs = require("fs");

const getTabSize = (layer) => {
    let str = "";
    for (let i = 0; i < layer; i++) {
        str += "\t";
    }

    return str;
}

const generateStructure = (layer, structure, key) => {
    const { description, type, content } = structure;

    let string = "";

    if (key) {
        string += getTabSize(layer);
        string += key + ": {\n";
    }
    string += getTabSize(layer + 1);
    string += 'description: "' + description + '",\n';
    string += getTabSize(layer + 1);
    string += 'type: "' + type + '",\n';

    switch (type) {
        case "object":
            string += generateProperties(layer + 1, content);
            break;
        case "array":
            string += generateArray(layer + 1, content);
            break;
    }

    if (key) {
        string += getTabSize(layer);
        string += "},\n";
    }

    return string;
}

const generateProperties = (layer, structure) => {
    let string = "";

    string += getTabSize(layer);
    string += "order: [" + Object.keys(structure).map(item => '"' + item + '"').join(", ") + "],\n"
    string += getTabSize(layer);
    string += "properties: {\n"
    Object.keys(structure).map((key) => {
        string += generateStructure(layer + 1, structure[key], key);
    })
    string += getTabSize(layer);
    string += "},\n"

    return string;
}

const generateArray = (layer, structure) => {
    let string = "";

    string += getTabSize(layer);
    string += "items: {\n"
    string += generateStructure(layer, structure);
    string += getTabSize(layer);
    string += "},\n"

    return string;
}


const start = (data) => {
    let schemaString = "const BufferPlus = require('buffer-plus');\n\n";

    schemaString += "enum SCHEMA {\n";

    Object.keys(data).map((key) => {
        const { schema } = data[key];

        schemaString += `\t${key} = "${schema}",\n`
    });

    schemaString += "}\n\n"

    Object.keys(data).map((key) => {
        const { schema, structure, description } = data[key];

        structure.description = description;
        schemaString += "BufferPlus.createSchema(SCHEMA." + key + ", {\n"
        schemaString += generateStructure(0, structure);
        schemaString += "})\n\n";
    })

    return schemaString;
}

module.exports = start