// 可能是我的node版本问题，不用严格模式使用ES6语法会报错
const db = require('../db');
const express = require('express');
const router = express.Router();
const fs = require("fs");
const parse = require("../helper/parse");

const generate = ({
	type,
	name,
	desc,
	content
}) => {
	// array or object
	if (content.length > 0) {
		if (type === "array") {
			return {
				type,
				description: desc,
				content: generate(content[0]),
			}
		} else {
			const json = {};

			content.map((item) => {
				json[item.name] = generate(item);
			})

			return {
				type,
				description: desc,
				content: json
			}
		}
	} else {
		return {
			type,
			description: desc,
		}
	}
}

router.post("/api/SDK/parse", (req, res) => {
	const {
		data
	} = req.body;


	let json = {};
	data.map(({
		schema,
		structure
	}) => {
		const {
			name,
			code,
			desc,
		} = schema;
		const {
			type
		} = structure;

		let content = {};

		structure.content.map((item) => {
			content[item.name] = generate(item);
		})

		json[name] = {
			schema: code,
			description: desc,
			structure: {
				type,
				content,
			}
		}
	})

	const schemaString = parse(json);


	fs.writeFile('./example.ts', schemaString, "utf8", err => {
		if (err) {
			console.log('Error writing file', err)
		} else {
			console.log('Successfully wrote file');

			res.download("./example.ts");
		}
	})

	// .catch(err => {
	// 	console.error(err.message);
	// 	res.send({
	// 		result: 0,
	// 		errorMsg: err.message
	// 	})
	// });
});

/**
 * 取得整個 document list
 */
router.get("/api/Mahjong/G6", (req,res) => {
	db.queryDocumentList().then((cursor) => {
		const keyList = cursor._result.map((item) => {
			return item._key;
		})

		res.send(keyList);
	})
})

/**
 * 取得特定document
 */
router.get("/api/Mahjong/G6/:id", (req, res) => {
	const {
		id
	} = req.params;

	db.queryDocument(id).then((doc) => {
		console.log('Document:', doc)

		res.send(doc);
	}, (err) => {
		console.error('Failed to fetch document:', err)
		res.send("Failed to fetch document:" + err);
	})
})

/**
 * 修改部分資料
 */
router.put("/api/Mahjong/G6/", (req, res) => {
	const {
		param
	} = req.body;

	const {
		key,
		data
	} = param;

	db.updateDocument(key, data).then((doc) => {
		console.log('Document:', doc)

		return db.queryDocument(key);
	}, (err) => {
		console.error('Failed to fetch document:', err)
		res.send("Failed to fetch document:" + err);
	}).then((doc) => {
		console.log('Document:', doc)

		res.send(doc);
	}, (err) => {
		console.error('Failed to fetch document:', err)
		res.send("Failed to fetch document:" + err);
	})
})

/**
 * create document
 */
router.post("/api/Mahjong/G6/", (req, res) => {
	const {
		key
	} = req.body;

	db.createDocument(key).then((err) => {
		if(err) {
			res.send({result: 1, msg: err})
		}
		else {
			res.send({result: 0})
		}
	})
})

router.delete("/api/Mahjong/G6/:id", (req, res) => {
	const {
		id
	} = req.params;

	
	db.deleteDocument(id).then((err) => {
		if(err) {
			res.send({result: 1, msg: err})
		}
		else {
			res.send({result: 0})
		}
	})
})

module.exports = router;
