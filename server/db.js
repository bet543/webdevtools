const {
	Database,
	aql
} = require("arangojs");
const { ARANGODB_URL } = require("./settings");

const db = new Database(ARANGODB_URL);

db.useDatabase("Database");

const collection = db.collection("CheatTable");



module.exports = {
	createDocument: async (key) => {
		doc = {
			_key: key,
			diceList: [3, 3],
			wall: [14, 15, 16, 1, 16, 16, 16, 17, 17, 17, 17],
			handList: [
				[1, 1, 25, 25, 25, 25, 3, 3, 3, 3, 11, 12, 13],
				[4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 11],
				[7, 7, 7, 7, 8, 8, 8, 8, 9, 9, 9, 9, 11],
				[21, 21, 21, 21, 22, 22, 22, 22, 23, 23, 23, 23, 24, 1]
			]
		}

		return collection.save(doc).then(
			meta => console.log("Document saved:", meta._rev),
			err => "Failed to save document:" + err
		  );
	},
	queryDocumentList: () => {
		return collection.all();
	},
	queryDocument: (key) => {
		return collection.document(key);
	},
	updateDocument: (key, data) => {
		return collection.update(key, data);
	},
	deleteDocument: (key) => {
		return collection.remove(key).then(
			() => console.log(key + " document removed"),
			err => "Failed to remove document" + err
		  );
	}
};
